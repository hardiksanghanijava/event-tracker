package com.anblicks.et.service;

import com.anblicks.et.entity.User;
import com.anblicks.et.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserService {
    @Autowired
    private UserRepository repository;

    public User saveUser(User user) {
        return repository.save(user);
    }

    public List<User> getUsers() {
        return repository.findAll();
    }

    public String deleteUser(int id) {
        repository.deleteById(id);
        return "User removed !! " + id;
    }

    public User updateUser(User user) {
    	User existingUser = repository.findById(user.getId());
    	existingUser.setName(user.getName());
    	existingUser.setUserName(user.getUserName());
    	existingUser.setEmail(user.getEmail());
        return repository.save(existingUser);
    }


}