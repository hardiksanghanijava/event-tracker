package com.anblicks.et.service;


import com.anblicks.et.entity.Category;
import com.anblicks.et.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryService {
    @Autowired
    private CategoryRepository repository;

    public Category saveCategory(Category category) {
        return repository.save(category);
    }

    public List<Category> getCategories() {
        return repository.findAll();
    }

    public String deleteCategory(int id) {
        repository.deleteById(id);
        return "Category removed !! " + id;
    }

    public Category updateCategory(Category category) {
    	Category existingCategory = repository.findById(category.getId());
    	existingCategory.setName(category.getName());
    	existingCategory.setDescription(category.getDescription());
        return repository.save(existingCategory);
    }


}