package com.anblicks.et.controller;

import com.anblicks.et.entity.Category;
import com.anblicks.et.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
public class CategoryController {

    @Autowired
    private CategoryService service;

    @PostMapping("/addCategory")
    public Category addCategory(@RequestBody Category category) {
        return service.saveCategory(category);
    }

    @GetMapping("/categories")
    public List<Category> findAllCategories() {
        return service.getCategories();
    }

    @PutMapping("/update-category")
    public Category updateCategory(@RequestBody Category category) {
        return service.updateCategory(category);
    }

    @DeleteMapping("/delete-category/{id}")
    public String deleteCategory(@PathVariable int id) {
        return service.deleteCategory(id);
    }
}